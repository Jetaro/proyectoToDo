package cl.ubb.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.ToDoDao;
import cl.ubb.model.ToDo;

public class ToDoService {
	
	@Autowired
	private  ToDoDao toDoDao;

	public ToDo crearToDo(ToDo todo) {
		// TODO Auto-generated method stub
		return toDoDao.save(todo);
	}

	public ToDo obtenerToDo(long id) {
		// TODO Auto-generated method stub
		ToDo todo = new ToDo();
		todo = toDoDao.findOne(id);
		return todo;
	}

	public ToDo obtenerToDo(String categoria) {
		// TODO Auto-generated method stub
		ToDo todo = new ToDo();
		todo = toDoDao.findByCategoria(categoria);
		return todo;
	}

	public ToDo obtenerToDo(boolean estado) {
		// TODO Auto-generated method stub
		ToDo todo = new ToDo();
		todo = toDoDao.findByEstado(estado);
		return todo;
	}

	public List<ToDo> obtenerTodos() {
		// TODO Auto-generated method stub
		List<ToDo> mistodos = new ArrayList<ToDo>();
		
		mistodos = (ArrayList<ToDo>) toDoDao.findAll();
		
		return mistodos;
	}
	

	
	
	
}
