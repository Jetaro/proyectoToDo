package cl.ubb.dao;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.ToDo;

public interface ToDoDao extends CrudRepository<ToDo,Long>{

	public ToDo findByCategoria(String string);

	public ToDo findByEstado(boolean b);

}
