package cl.ubb;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.ToDoDao;
import cl.ubb.model.ToDo;
import cl.ubb.service.ToDoService;


@RunWith(MockitoJUnitRunner.class)
public class ToDoServiceTest {

	@Mock
	private ToDoDao toDoDao;
	
	@InjectMocks
	private ToDoService toDoService;
	
	@Test
	public void ObtenerToDoPorId() {
		
		//arrange
		ToDo todo = new ToDo();
		//todo.setID(1);
		ToDo todoRetornado = new ToDo();
		
		//act
		when(toDoDao.findOne(anyLong())).thenReturn(todo);
		todoRetornado = toDoService.obtenerToDo(todo.getID());
		
		//assert
		Assert.assertNotNull(todoRetornado);
		Assert.assertEquals(todoRetornado, todo);
	}
	
	@Test
	public void ObtenerToDoPorCategoria() {
		
		//arrange
		ToDo todo = new ToDo();
		todo.setCategoria("ToDo Universidad");
		ToDo todoRetornado = new ToDo();
				
		//act
		when(toDoDao.findByCategoria("ToDo Universidad")).thenReturn(todo);
	    todoRetornado = toDoService.obtenerToDo(todo.getCategoria());
		
		//assert
		Assert.assertNotNull(todoRetornado);
		Assert.assertEquals(todoRetornado, todo);
	}
	
	@Test
	public void ObtenerToDoPorEstado() {
		
		//arrange
		ToDo todo = new ToDo();
		todo.setEstado(true);
		ToDo todoRetornado = new ToDo();
				
		//act
		when(toDoDao.findByEstado(true)).thenReturn(todo);
	    todoRetornado = toDoService.obtenerToDo(todo.isEstado());
		
		//assert
		Assert.assertNotNull(todoRetornado);
		Assert.assertEquals(todoRetornado, todo);
	}
	
	@Test
	public void ObtenerTodosLosLibros() {
		
		//arrange
		List<ToDo> mistodos = new ArrayList<ToDo>();
		List<ToDo> resultado = new ArrayList<ToDo>();
		
		//act
		when(toDoDao.findAll()).thenReturn(mistodos);
		resultado = toDoService.obtenerTodos();
		
		//assert 
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultado, mistodos);
	}
	
	/*
	@Test
	public void deboCrearUnToDoYRetornarlo(){
		
		        //arrange
				ToDo todo = new ToDo(); 
				ToDo todoCreado = new ToDo();
				
				//act
				when(toDoDao.save(todo)).thenReturn(todo);
				todoCreado = ToDoService.crearToDo(todo);
				
				//assert
				Assert.assertNotNull(todoCreado);
				Assert.assertEquals(todo,todoCreado);
	}
	
	*/
	
	/*
	@Test
	public void test() {
		
	}
     */
}
